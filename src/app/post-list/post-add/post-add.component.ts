import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {PostsService} from '../../services/posts.service';
import {Router} from '@angular/router';
import {Post} from '../../models/post.model';
import * as firebase from 'firebase';
@Component({
  selector: 'app-post-add',
  templateUrl: './post-add.component.html',
  styleUrls: ['./post-add.component.scss']
})
export class PostAddComponent implements OnInit {

  postForm: FormGroup;

  constructor(private formBuilder: FormBuilder,
              private postsService: PostsService,
              private router: Router) { }

  ngOnInit() {
    this.initForm();
  }

  initForm(){
    this.postForm = this.formBuilder.group({
      title: ['', Validators.required],
      content: ['', Validators.required]
    });
  }

  onSavePost(){
    const title = this.postForm.get('title').value;
    const content = this.postForm.get('content').value;
    const nowDate = new Date();
    const timestamp = nowDate.getTime();
    //const nowDate2 = firebase.firestore.Timestamp.fromDate(nowDate);
    const newPost = new Post(title, content, 0, timestamp);
    this.postsService.createPost(newPost);
    this.router.navigate(['/posts']);
  }

}
