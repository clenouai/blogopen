import {Component, OnDestroy, OnInit} from '@angular/core';
import {Post} from '../models/post.model';
import {Subscription} from 'rxjs';
import {PostsService} from '../services/posts.service';
import {Router} from '@angular/router';
import { DatePipe} from '@angular/common';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.scss']
})
export class PostListComponent implements OnInit, OnDestroy {

  posts: Post[];
  post: Post;
  postSub: Subscription;

  constructor(private postsService: PostsService,
              private router: Router) {  }

  ngOnInit() {
    this.postSub = this.postsService.postsSubject.subscribe(
        (posts: Post[]) => {
          this.posts =  posts;
          //console.log(this.post);
        }
    )
    this.postsService.getPosts();
    this.postsService.emitPosts();
    console.log(this.posts);
  }

  newPost(){
    this.router.navigate(['/posts', 'new']);
  }

  getColor(i: number){
    this.post = this.posts[i];
    if(this.post.loveIts > 0) {
      return 'green';
    } else if(this.post.loveIts < 0) {
      return 'red';
    }
  }

  deletePost(post: Post) {
    this.postsService.deletePost(post);
  }

  likeIt(i: number){
    this.post = this.posts[i];
    this.post.loveIts = this.post.loveIts + 1;
    this.postsService.updatePost(this.post);
    this.postsService.emitPosts();
    return this.post.loveIts ;
  }


  dontLikeIt(i: number){
    this.post = this.posts[i];
    this.post.loveIts = this.post.loveIts - 1;
    this.postsService.updatePost(this.post);
    this.postsService.emitPosts();
    return this.post.loveIts ;
  }

  ngOnDestroy(){
    this.postSub.unsubscribe();
  }


}
