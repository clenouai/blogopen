import { Component } from '@angular/core';
import * as firebase from 'firebase';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'blog';
  constructor(){
    var config = {
      apiKey: "AIzaSyBRZGKK3QYPUN3zbH5KNbIfC6fkrxI3aTQ",
      authDomain: "blog-1f08f.firebaseapp.com",
      databaseURL: "https://blog-1f08f.firebaseio.com",
      projectId: "blog-1f08f",
      storageBucket: "blog-1f08f.appspot.com",
      messagingSenderId: "681823081866"
    };
    firebase.initializeApp(config);
  }
}
