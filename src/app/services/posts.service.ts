import {Post} from '../models/post.model';
import {Subject} from 'rxjs';
import * as firebase from 'firebase';
import DataSnapshot = firebase.database.DataSnapshot;

export class PostsService {

    posts: Post[] = [];
    postsSubject = new Subject<Post[]>();

    constructor(){
        this.getPosts();
    }

    emitPosts(){
        this.postsSubject.next( this.posts );
    }

    savePosts(){
        firebase.database().ref('/posts').set(this.posts);
    }

    getPosts(){
        firebase.database().ref('/posts')
            .on('value', (data: DataSnapshot) => {
                this.posts = data.val() ? data.val() : [];
                this.emitPosts();
            });
    }

    createPost( post: Post){
        this.posts.push(post);
        this.savePosts();
        this.emitPosts();
    }

    updatePost(post: Post){
        const id = this.posts.findIndex(
            (postElement) => {
                if(postElement === post){
                    return true;
                }
            } );
        firebase.database().ref('/posts/' + id).update({ loveIts: post.loveIts });
        this.emitPosts();
    }

    deletePost(post: Post){
        const index = this.posts.findIndex(
            (postElement) => {
                if (postElement === post) {
                    return true;
                }
            }
        );
        this.posts.splice(index, 1);
        this.savePosts();
        this.emitPosts();
    }

}
